package com.apitechu.pruebagit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebagitApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebagitApplication.class, args);
	}

}
